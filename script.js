const  wrapper = document.querySelector(".wrapper");
const  submit = document.querySelector(".add-button");
const  task = document.querySelector("#task");
const  blank = document.querySelector(".empty-list");
const  hide = document.querySelector(".hide-container");
const  hideCompleted = document.querySelector(".hide-completed");
const list = document.querySelector(".todo-list");
const maxLength = document.querySelector(".max-length");
const cancel = document.querySelector(".cancel");
const deleteConfirmation = document.querySelector(".delete-confirmation");
const clickArea = document.querySelector(".click-area");


task.addEventListener("input", (e) => {

    if (task.value.length > 54) {
        task.style.border = '1px solid #FF3104';
        maxLength.style.display = 'inline';
        submit.style.marginBottom = 19 + 'px';
    }else {
        task.style.border = '1px solid #FFCD04';
        maxLength.style.display = 'none';
        submit.style.marginBottom = '0';
    }
})

const cancelIcon = '&#10005;';

let todos = [];

let localTodos = localStorage.getItem("todos");

if(localTodos){
    todos = JSON.parse(localTodos);

    render(todos);
}

submit.addEventListener("click", (e) => {
    e.preventDefault();

    if (task.value.length == 0 || task.value.length > 54){
        return;
    }

    const obj = {
        id: new Date().getTime(),
        text: task.value,
        completed: false
    };
    todos.push(obj);

    localStorage.setItem("todos", JSON.stringify(todos));
    render(todos);
})

function showDeleteConfirmation(id) {
    task.style.background = 'rgba(0, 0, 0, 0.025)';
    wrapper.style.background = 'rgba(0, 0, 0, 0.25)';


    deleteConfirmation.innerHTML = `
        <div class="delete-container">
            <span class="confirm-text">Are you sure you want to delete?</span>

            <div class="confirm-container">
                <button type="submit" class="confirm" onclick="deleteTodo(${id})">Yes</button>
                <button type="reset" class="deny" onclick="showDeleteConfirmationCancel()"  >No</button>
            </div>
        </div>
    `;


    clickArea.classList.toggle('true');
    clickOutside();
}



function showDeleteConfirmationCancel(){
    deleteConfirmation.innerHTML = ""
    wrapper.style.background = "none";
    task.style.backgroundColor = 'white';
    clickArea.classList.remove('true');
}

function deleteTodo(id) {

    todos = todos.filter(e =>  id != e.id);

    render(todos);

    showDeleteConfirmationCancel();

    localStorage.setItem("todos", JSON.stringify(todos));

}

const changeCompleted = (id) => {
      todos.map(element => {
        if(element.id === id){
             element.completed = !element.completed;
        }
    })

    render(todos);
    localStorage.setItem("todos", JSON.stringify(todos));

}

hideCompleted.addEventListener("click", (e) => {
     render(todos);
})


function render (todos, t = false)  {
    list.innerHTML = "";


    if(todos.length === 0) {
        blank.className = "empty-list";
        hide.style.display = 'none';
        if(t){
            hide.style.display = 'flex';
        }
        return
    }
         blank.className = "empty-list true";

        todos.forEach(element => {

            if (element.completed) {
                if(!hideCompleted.checked) {
                    list.innerHTML += `<div class="todo-hover">
            <li class="todo" >
                <div class="item">
                    <input type="checkbox" class="hide-completed" id="list-check" checked onclick="changeCompleted(${element.id})" >
                    <span style="color: #ACACAC" class="list-item">${element.text}</span>
                </div>
                <span class="delete" id=${element.id} onclick="showDeleteConfirmation(${element.id})">${cancelIcon}</span>
            </li>
        </div>`;
                }
            } else {
                list.innerHTML += `<div class="todo-hover">
            <li class="todo">
                <div class="item">
                    <input type="checkbox" class="hide-completed" id="list-check" onclick="changeCompleted(${element.id})" >
                    <span class="list-item">${element.text}</span>
                </div>
                <span class="delete" id=${element.id} onclick="showDeleteConfirmation(${element.id})">${cancelIcon}</span>
            </li>
        </div>`;
            }
        })
    hide.style.display = 'flex';
    task.value = '';
    cancel.style.display = 'none';
}

function clickOutside () {
    clickArea.addEventListener("click", () => {
        showDeleteConfirmationCancel();
    })
}

function showCancelIcon() {
    task.addEventListener('click', () => {
        cancel.style.display = 'inline';
    })
}

function hideCancelIcon() {
    cancel.addEventListener('click', () => {
        task.value = '';
        x.style.display = 'none';
        task.style.border = '1px solid #FFCD04';
        maxLength.style.display = 'none';
        submit.style.marginBottom = '0';
    })
}
showCancelIcon();
hideCancelIcon();


